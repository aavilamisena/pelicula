$(document).ready(function(){
	read();
	leerGen();

	$('#btnAdd').click(function(e){

		//formato.productID = productID;

		e.preventDefault();
		var formdata = new FormData($('#frmPelicula')[0]);
		
	     // var formulario = $('#frmPelicula').serialize();
	     // console.log(formulario);
	     //console.log($("#frmPelicula")[0]);
	     //var datos = new FormData($("#frmPelicula")[0]);
	    
	     var datos = $('#frmPelicula').serialize();
		$.ajax({
            url: '../controlador/pelicula.create.php',
            type: 'POST',
            dataType: 'JSON',
            data: formdata,
            cache: false,
            contentType: false,
            processData: false,
            
            success: function (data) {
            	alert(data);
            	console.log("success");
            },
            error: function(xhr){
     	 
     	 		console.log("An error occured: " + xhr.status + " " + xhr.statusText);
			}

        })
	});

	$('.formato').click(function(event) {
		console.log('formato');
		var cantidad = new Array()
		var menu = "<input type='number' id = 'punto' name='cantidad[]'>";

		if (this.checked) {
			//console.log('ljl');
			$(this).siblings('#cantidadPelicula').append(menu);
		} else  {
			console.log('false');
			//alert('false');
			$(this).siblings('#cantidadPelicula').children('#punto').remove();
		}

		

	});

	// autocompletar

	$('#productora').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: '../controlador/productora.consulta.php',
				type: 'GET',
            	dataType: 'JSON',
            	data: {term: request.term},

				success: function(data) {
					response(data);
				},
				error: function(xhr){
     	 
     			 console.log("An error occured: " + xhr.status + " " + xhr.statusText);
			}
			})
		},
		select: function(event, ui) {
			event.preventDefault();
			//console.log(ui);
			$('#productora').val(ui.item.label);
			$('#idProductora').val(ui.item.value);
		}
	});

	// read
	//

	

	 
	function read() {
		
		var parametros = {
	        
	        "accion": "CONSULTA_TODO"
	    }

		$.ajax({
			url: '../controlador/pelicula.read.php',
			type: 'POST',
			dataType: 'json',
			data: parametros,
		})
		.done(function(json) {
			console.log("success");
			//console.log(json);
			var claseimagen = "imagenTablaPelicula";
			var tabla = null;

			//console.log(json);
			

				$('#peliculaJson').DataTable({
					data: json,
					

					bJQueryUI: true,
					language: {"url": '../recursos/lenguages/dataTableSpanish.json' },

					

					columns: [
				
			            { "data": "nombre" },
			            { "data": "nombreGenero" },
			            { "data": "nombre_formato" },
			            { "data": "cantidad" },
			            
			            { "data": "productora_nombre" },
			            // { "data": "imagen" },
			            { "title": "Imagen", "data": "imagen" },

			            {
			            	"data": "userId"
			            }
			      			           
			        ],

			       
			        "aoColumnDefs": [
			           {
			                "aTargets": [6],
			                "mData": "userId",
			                "mRender": function (data, type, full) {
			                    return '<button href="#"  class = "punto" onclick="consultaId('+ full.dirPelicula + ')"  data-toggle="modal" data-target="#myModal">Editar</button>';
			                }
			            },

			            {
			                "aTargets": [5],
			                "mData": "imagen",
			                "mRender": function (data, type, full) {
			                    return '<img src="'+ full.imagen +'"  class="'+claseimagen +'">';
			                }
			            }


			         ],

			          dom: 'Bfrtip',
				        buttons: [
				            {
				                extend: 'pdfHtml5',
				                orientation: 'landscape',
				                pageSize: 'LEGAL',
				                download: 'OPEN',
				                text: '<i class="far fa-file-pdf"></i>'
				            },
				            {
					            extend: 'copy',
					            text: '<i class="far fa-copy"></i>',
					            exportOptions: {
					                modifier: {
					                    page: 'current'
					                }
					            }
					        },

					         {
					         	extend: 'colvis',
                 				text: '<i class="fas fa-filter" style = "color:blue"></i>'
                 			 },

                 			 {
					         	extend: 'print',
                 				text: '<i class="fas fa-print" style = "color:blue"></i>'
                 			 }



				        ]


				});
				
   			
		})
		.fail(function() {
			console.log("error al cargar peliculas");
		})
	} 
// fin de read()


	

	function leerGen() {
		$.ajax({
			url: '../controlador/genero.read.php',
			type: 'POST',
			data: null,
			dataType: 'JSON',
		}).done(function(json) {
			//console.log(json);

			var genero;
			$.each(json['arrayFormato'], function(index, val) {

				genero += '<option value = " '+ val.idGenero + '" > '+ val.nombreGenero +'</option>'

			 });

			$('#genero').html(genero)

		})
	}

	function imagen() {
		// body...
	}
 })
 
 function consultaId(idPeliculas) {
  var myWindow = idPeliculas;
 

  var parametros = {
        "idFormulario" : idPeliculas,
        "accion": "CONSULTA_POR_ID"
    }

    
    $.ajax({
        
        url: '../controlador/pelicula.read.php',
        
        type: "post",
        dataType: 'JSON',
        data: parametros

    }).done(function(json) { 
        
        	console.log("exito modal");
        	console.log(json);
        	 $('#nombrePeliculamodal').val(json[0].nombre);

        	// if (json.length == 1) {
        		
        	// 	 $('#nombrePeliculamodal').val(json.nombre);


        	// } else {
        		
        	// }
            // $('#modal-modificar').modal('show');
            // $('#nombrePelicula').val(response.idFormulario); 
            // $('#genero').val(response.idCarpeta);
            // $('#productora').val(response.nombre);
            // $('#txtRuta').val(response.ruta);
            // $('#txtIcono').val(response.icono);
            // $('#ddlEstado').val(response.estado);
            
        }).fail(function() {
			console.log("error al cargar peliculas modal");
		})

       
    }
 
  

 
