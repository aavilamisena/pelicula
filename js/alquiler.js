$(document).ready(function(){
	var peliculas = new Array();

	if (localStorage.getItem('datos')) { 

		peliculas = JSON.parse(localStorage.getItem('datos'));
    $('#contadorCar').html(peliculas.length);

	}

	

	 $(".modelo").attr('data-toggle', 'modal');
	 $(".modelo").attr('data-target', '#exampleModalCenter');
	 var titulos = $(".card-title").text();
	 //console.log(titulos);

  $(".modelo").click(function(){

    var ruta = $(this).parent('div.card-body').siblings('.card-img-top').attr("src");
    var titulo = $(this).parent().children(".card-title").text();
    

    $("#modal_img").attr("src", ruta);
    $('#exampleModalLongTitle').text(titulo);

    $.ajax({
  		url: '../controlador/formato.read.php',
        type: 'POST',
        dataType: 'JSON',
        data: null,

  		}).done(function (json) {
                     // console.log(json.arrayFormato);
                    //obtenerUsuarios();
                    var select = null;
                    $.each(json.arrayFormato, function(index, val) {
                        select += '<option value ="' +val.porcentaje + '">'+val.nombre_formato +' </option>'
                    	//console.log(val.porcentaje);
                    });

                    $('#selFormato').html(select);
                   

        }).fail(function () {
                    console.log("error");
                }) 

        // formato
        
        $.ajax({
  		url: '../controlador/tiempo.read.php',
        type: 'POST',
        dataType: 'JSON',
        data: null,

  		}).done(function (json) {
                   
                    //console.log(json.arrayFormato);
                    //obtenerUsuarios();
                    var select1 = null;
                    $.each(json.arrayFormato, function(index, val) {
                        select1 += '<option value ="' +val.precio + '  ">'+val.descripcion +' - '+val.precio +' </option>'
                    	//console.log(val);
                    });

                    $('#selTiempo').html(select1);
                   

        }).fail(function () {
                    console.log("error");
                }) 

        // tiempo alquiler
		        

  }); // fin boton pelicula hacia modal

// boton para enviar al localstorage
var contador = 0;
  $('.alquilar').click(function() {
  		
  		guardarLS();
  		contador += 1;

  		
  }); //fin boton alquilar

		
		
  var totalPedido = 0;		
  function guardarLS() {

  		var titulo =  $('#exampleModalLongTitle').text();
  		var formato = $('#selFormato option:selected').val();
  		var precio = $('#selTiempo option:selected').val();
      	var ruta = $('#modal_img').attr("src");
      	var porcentaje = $('#selFormato option:selected').val();

      	
      	var precioPeli = (porcentaje * precio )/100;
      	totalPedido += precioPeli; 
      	//console.log(precioPeli);

  		let pelicula = {
  			titulo:  $('#exampleModalLongTitle').text(),
  			formato: $('#selFormato option:selected').text(),
  			tiempo: $('#selTiempo option:selected').text(),
  			ruta: $('#modal_img').attr("src"),
  			precio: precioPeli,
  			total: totalPedido
  			
  		}
      	//console.log(ruta);

  		peliculas.push(pelicula);
  		console.log(peliculas);

  		localStorage.setItem('datos', JSON.stringify(peliculas));

  		$('#contadorCar').html(peliculas.length);

  	
  		
  }
   $('#btnCarrito').click(function() {
      //console.log("hola");
      var itemCar = "";
      var total = 0;
       $.each(peliculas, function(index, val) {

       	  total += val.precio	
         // console.log(val.ruta);
          itemCar += '<div class="row">';
          itemCar += '<div class=col-6>';
           itemCar += '<img src="'+val.ruta +'" class=card-img-top carrito_sd>'; 
           itemCar += '</div>';

           itemCar += '<div class=col-6>';
              itemCar += '<div class=col-12>';
              	itemCar += '<span>'+val.titulo +'</span>';
              itemCar += '</div>';

              itemCar += '<div class=col-12>';
              	itemCar += '<span>'+val.formato +'</span>';
              itemCar += '</div>';

              itemCar += '<div class=col-12>';
              	itemCar += '<span>'+val.precio +'</span>';
              itemCar += '</div>';
           itemCar += '</div>';
            itemCar += '</div>';

       });
       $('#modalCar').html(itemCar);
       
       $('#total_alquiler').html(total);

       
      
  }); //fin boton carrito


   $('#alquilarDb').click(function() {
   		console.log("peliculas desde alquiler a db", peliculas);
   		
   		//alert("hola");
   		$.ajax({
  		url: '../controlador/alquiler.crear.php',
        type: 'POST',
        dataType: 'JSON',
        data: {datos:peliculas},

  		}).done(function (json) {
                   
              console.log("funciona");                        

        }).fail(function () {
              console.log("error");
                }) 

   }); //fin alquilarDb
   
  
	
	$("#selFormato").change(function(){
			var porcentaje = $('#selFormato option:selected').val();
			var precio = $('#selTiempo option:selected').val();
			var valor = calculoPrecio(porcentaje, precio);
			console.log(valor);
			$('#precioPeliculaModal').html("$"+valor);
	
	});

	$("#selTiempo").change(function(){
		 	var porcentaje = $('#selFormato option:selected').val();
			var precio = $('#selTiempo option:selected').val();
			var valor = calculoPrecio(porcentaje, precio);
			console.log(valor);
			$('#precioPeliculaModal').html("$"+valor);

	});

	function calculoPrecio(valor1, valor2) {
		
		return (parseInt(valor1) * parseInt(valor2))/100 + parseInt(valor2);
	}

});
