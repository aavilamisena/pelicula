$(document).ready(function(){
	read();
	leerGen();

	$('#btnAdd').click(function(){


		var formdata = new FormData($('#frmImagen')[0]);
		// var file_data = $('#imagenPeli').prop('files')[0];   
	    // var form_data = new FormData();                  
	    // form_data.append('file', file_data);
	     // console.log(file_data);
	     // var formulario = $('#frmPelicula').serialize();
	     // console.log(formulario);
	     //console.log($("#frmPelicula")[0]);
	     //var datos = new FormData($("#frmPelicula")[0]);
	     var parametros = {
	     	"datos" : datos,
	     	"imagen" : formdata
	     };
	     var datos = $('#frmPelicula').serialize();
		$.ajax({
            url: '../controlador/pelicula.create.php',
            type: 'POST',
            dataType: 'JSON',
            data: datos,
            
            success: function (data) {
            	console.log("success");
            },
            error: function(xhr){
     	 
     	 		console.log("An error occured: " + xhr.status + " " + xhr.statusText);
			}

        })
	});

	// autocompletar

	$('#productora').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: '../controlador/productora.consulta.php',
				type: 'GET',
            	dataType: 'JSON',
            	data: {term: request.term},

				success: function(data) {
					response(data);
				},
				error: function(xhr){
     	 
     	 console.log("An error occured: " + xhr.status + " " + xhr.statusText);
			}
			})
		},
		select: function(event, ui) {
			event.preventDefault();
			//console.log(ui);
			$('#productora').val(ui.item.label);
			$('#idProductora').val(ui.item.value);
		}
	});

	// read
	//
	 
	function read() {
		$.ajax({
			url: '../controlador/pelicula.read.php',
			type: 'POST',
			dataType: 'json',
			data: null,
		})
		.done(function(json) {
			console.log("success");
			//console.log(json);

			var tabla = null;

			$.each(json, function(index, val) {

				 tabla += '<tr>';
				 tabla += '<th>'+(index+1)+'</th>';
				 tabla += '<td>'+ val.nombre + '</td>';
				 tabla += '<td>'+ val.genero + '</td>';
				 tabla += '<td>'+ val.formato + '</td>';
				 tabla += '<td>'+ val.cantidad + '</td>';
				 tabla += '<td>'+ val.id_productora + '</td>';
				 tabla += '<td>  opciones</td>';
				 tabla += '</tr>'
                
                      
			});
			$('#tableBodyPelicula').html(tabla);
		})
		.fail(function() {
			console.log("error");
		})
		
	}

	function leerGen() {
		$.ajax({
			url: '../controlador/genero.read.php',
			type: 'POST',
			data: null,
			dataType: 'JSON',
		}).done(function(json) {
			//console.log(json);

			var genero;
			$.each(json['arrayFormato'], function(index, val) {

				genero += '<option value = "{{val.idGenero }}" > '+ val.nombreGenero +'</option>'

			 });

			$('#genero').html(genero)

		})
	}

	function imagen() {
		// body...
	}
 })
