function cargarModal(idUsuario) {
    document.getElementById('txtIdClienteModalM').value = idUsuario;
    
}

function eliminarModal(cliIdentificacion) {
    document.getElementById("txtIdClienteModalE").value = cliIdentificacion;
}

$(document).ready(function () {
    obtenerUsuarios();

    /**
     * Función para agregar usuarios al sistema
     */
    $(document).on('click', '#btnAdd', function () {
        $.ajax({
            url: '../controlador/agregarUsuario.php',
            type: 'POST',
            dataType: 'JSON',
            data: $('#formularioUsuario').serialize(),
        })
                .done(function (json) {
                    alert(json.mensaje);
                    console.log("success");
                    obtenerUsuarios();
                })
                .fail(function () {
                    console.log("error");
                })
    });
    
    /**
     * Función para eliminar usuarios del sistema
     */
    $(document).on('click', '#btnBorrar', function () {
        $.ajax({
            url: '../controlador/eliminarUsuario.php',
            type: 'POST',
            dataType: 'JSON',
            data: $('#modalEliminar').serialize()
        })
                .done(function (json) {
                    alert("paseeeeeeee");
                    console.log("success");
                    obtenerUsuarios();
                })
                .fail(function (json) {
                    alert(json.mensaje);
                    console.log("error");
                })
    });

    /**
     * Función para obtener un usuario por identificacion
     */
    $(document).on('change', '#txtIdentificacion', function () {
        $.ajax({
            url: '../controlador/obtenerUsuario.php',
            type: 'POST',
            dataType: 'JSON',
            data: $('#formularioUsuario').serialize(),
        })
                .done(function (json) {
                    $('#mensaje').html("<p>Identificación ya registrada</p>");
                    console.log("success");
                    obtenerUsuarios();
                })
                .fail(function () {
                    console.log("error");
                })
    });

    /**
     * Función para obtener los usuarios
     * @returns {undefined}
     */
    function obtenerUsuarios() {
        $.ajax({
            url: '../controlador/cargarUsuario.php',
            type: 'POST',
            dataType: 'JSON',
            data: null,
        })
                .done(function (json) {
                    var table;
                    table = '<table id="listadoClientes" class="table-fill">';
                    table += '<thead>';
                    table += '<tr>';
                    table += '<th class="text-left">Identificación</th>';
                    table += '<th class="text-left">Nombre</th>';
                    table += '<th class="text-left">Apellido</th>';
                    table += '<th class="text-left">Fecha nacimiento</th>';
                    table += '<th class="text-left">opciones</th>';
                    table += '</tr>';
                    table += '</thead>';
                    table += '<tbody class="table-hover">';

                    $.each(json.arreglo, function (contador, fila) {
                        table += '<tr>';
                        table += '<td class="text-left">' + fila.cliIdentificacion + '</td>';
                        table += '<td class="text-left">' + fila.cliNombre + '</td>';
                        table += '<td class="text-left">' + fila.cliApellido + '</td>';
                        table += '<td class="text-left">' + fila.cliFechaNacimiento + '</td>';
                        table += '<td class="text-left"><a onclick="cargarModal(' + fila.idCliente + ')" data-toggle="modal" data-target="#modalModificar" id="btnModificar"><img src="https://png.icons8.com/nolan/40/000000/refresh.png"></a>&nbsp&nbsp';
                            table += '&nbsp<a onclick="eliminarModal(' + fila.cliIdentificacion + ')" data-toggle="modal" data-target="#modalEliminar" id="btnEliminar"><img src="https://png.icons8.com/nolan/40/000000/cancel.png"></a></td>';
                        table += '</tr>';
                    })
                    table += '</tbody>';
                    table += '</table>';
                    $('#tablaUsuario').html(table);

                    $('#listadoClientes').DataTable({
                        'ordering': true,
                        'order': [[0, 'desc']],

                        'language': {
                            'url': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json',

                        }
                    });

                })
                .fail(function () {
                    console.log("error");
                })
    }
});
