<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Alquiler Peliculas</title>
        <!-- Estilos que implementa el index -->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../css/alquiler.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../assets/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="../assets/css/buttons.dataTables.css"> 
      <!--   <link rel="stylesheet" type="text/css" href="../assets/fonts/fontawesome.css"> -->
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

        <!-- Scripts que implementa el index -->
        <script src="../assets/js/jquery.js" type="text/javascript"></script> 
       <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
        <script src="../assets/js/bootstrap.js" type="text/javascript"></script> 
        <script type="text/javascript" src="../js/pelicula.js"></script>
        <script type="text/javascript" src="../assets/js/jquery-ui.js"></script>

      <!-- Datatables --> 
        
        <script type="text/javascript" src="../assets/js/dataTable/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/dataTables.buttons.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/pdfmake.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/vfs_fonts.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/buttons.html5.min.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/buttons.colVis.min.js"></script>
        <script type="text/javascript" src="../assets/js/dataTable/buttons.print.js"></script>

       
       
    </head>
    <body>
      
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1>Peliculas</h1>
          </div>
        </div>
      <form id="frmPelicula">
        <div class="row">
          <div class="col-md-6 form-group">
            <label for="nombrePelicula">Nombre Pelicula</label>
            <input class="form-control" type="text" name="nombrePelicula" id="nombrePelicula" placeholder="ingrese nombre pelicula">
          </div>
          <div class="col-md-6 form-group">
             <label for="genero">Nombre genero</label>
             <select id="genero" class="form-control" name="genero">
               
             </select>
          </div>
        </div>

        <div class="row">
          <div class="col-md-6">
            <label for="imagen">subir imagen</label>
            <input type="file" id="imagenPeli" name="imagenPeli">
          </div>
          
          <div class="col-md-6 form-group">
            <label class="form-check-inline">Formato</label>
           <div class="form-check form-check-inline">
              <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="1">
              
              <label class="form-check-label" for="inlineCheckbox1">DVD</label>
              <div id="cantidadPelicula" ></div>

            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="2">
              <label class="form-check-label" for="inlineCheckbox2">BLUE</label>
              <div id="cantidadPelicula" ></div>
            </div>

             <div class="form-check form-check-inline">
              <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="3">
              <label class="form-check-label" for="inlineCheckbox2">BLUE-RAY-3D</label>
              <div id="cantidadPelicula" ></div>
            </div>
          </div>
        </div>


        <div class="row">

          <div class="col-md-6 form-group">
            <img src="" id="imagenPelicula" >
            
          </div>
         
          <div class="col-md-6 form-group">
            <label for="productora">Productora</label>
            <input type="text" id="productora" name="productora" class="form-control">
            <input type="hidden" name="idProductora" id="idProductora">
          </div>

         
        </div>
      </form>
        <div class="row">
          <div class="col-md-6">
            <button  type="button" class="btn btn-primary" name="" id="btnAdd"> Agregar</button>
          </div>
        </div>
      </br>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
                <table class="table table-striped" id="peliculaJson">
                    <thead>
                      <tr>
                        
                        <th scope="col">nombre</th>
                        <th scope="col">genero</th>
                        <th scope="col">formato</th>
                        <th scope="col">cantidad</th>
                        <th scope="col">productora</th>
                        <th scope="col">imagen</th>
                        <th scope="col">opcion</th>
                   
                      </tr>
                    </thead>
                  
                  
                </table>
            </div>
          
          </div>

        </div>
        


      </div>

      <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
          
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                
                <h4 class="modal-title">Modal Header</h4>
              </div>
              <div class="modal-body">
                    <div class="row" >
                        <div class="col-md-6 form-group">
                        <label for="nombrePeliculamodal">Nombre Pelicula</label>
                        <input class="form-control" type="text" name="nombrePeliculamodal" id="nombrePeliculamodal" placeholder="ingrese nombre pelicula">
                      </div>

                      <div class="col-md-6 form-group">
                         <label for="generomodel">Nombre genero</label>
                         <select id="generomodel" class="form-control" name="generomodel">
                           
                         </select>
                      </div>

                    </div>

                    <div class="row" >
                        <div class="col-md-6 form-group">
                          <label for="productoramodal">Productora</label>
                          <input type="text" id="productora" name="productora" class="form-control">
                          <input type="hidden" name="productoramodal" id="productoramodal">
                        </div>

                     <div class="col-md-6 form-group">
                        <label class="form-check-inline">Formato</label>
                       <div class="form-check form-check-inline">
                          <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="1">
                          
                          <label class="form-check-label" for="inlineCheckbox1">DVD</label>
                          <div id="cantidadPelicula" ></div>

                        </div>
                        <div class="form-check form-check-inline">
                          <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="2">
                          <label class="form-check-label" for="inlineCheckbox2">BLUE</label>
                          <div id="cantidadPelicula" ></div>
                        </div>

                         <div class="form-check form-check-inline">
                          <input class="form-check-input formato" type="checkbox" name="formato[]" id="inlineCheckbox1" value="3">
                          <label class="form-check-label" for="inlineCheckbox2">BLUE-RAY-3D</label>
                          <div id="cantidadPelicula" ></div>
                        </div>
                    </div>

                    </div>
                
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            
          </div>
        </div>


    </body>
</html>
