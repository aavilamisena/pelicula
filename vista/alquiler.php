<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Alquiler Peliculas</title>
        <!-- Estilos que implementa el index -->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/table.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../css/alquiler.css">
        <link rel="stylesheet" type="text/css" href="../css/mdb.min.css">
        <!--<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>-->

        <!-- Scripts que implementa el index -->
        <script src="../js/jquery.js" type="text/javascript"></script> 
       <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
        <script src="../js/bootstrap.js" type="text/javascript"></script> 
        
         <script type="text/javascript" src="../js/mdb.js"></script>
        <!--<script src="../js/jquery.dataTables.js" type="text/javascript"></script>-->
        <script type="text/javascript" src="../js/alquiler.js"></script>

    </head>
    <body>
       <nav class="navbar navbar-expand-sm bg-light">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Inicio</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Alquiler</a>
            </li>
           
            
          </ul>
        <div class="card-body">
            <button class="btn btn-primary float-right" id="btnCarrito" data-toggle="modal" data-target="#exampleModalPreview">
                carrito
                <span id="contadorCar"></span>
                
            </button>
        </div>
    </nav>
        <div class="container">
            <div class="row">
                
                   
                
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <h1>Alquiler</h1>
                </div>
     
            </div>

            <div>
                <div class="col-12" >
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Accion</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Terror</a>
                      </li>
                      <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Drama</a>
                      </li>
                      
                    </ul>
                    <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                              <div class="row">

                                  <div class="card-group" > 
                                      <div class="card" >
                                        <img class="card-img-top" src="../img/caratulas/accion/narnia.jpg"  alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Cronicas de Narnia</h5>
                                          <p class="card-text">Los cuatro hermanos Pevensie regresan a Narnia y, aunque para ellos ha pasado un año, allá han transcurrido 1300 años.</p>
                                          <p class="card-text"></p>
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card" >
                                        <img class="card-img-top" src="../img/caratulas/accion/elSeñorAnillos.jpg"  alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">El señor de los Anillos</h5>
                                          <p class="card-text">Basada en la primer historia épica de J.R.R. Tolkien, sobre la búsqueda para poseer o destruir todo el poder del Anillo.</p>
                                          <p class="card-text"></p>
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card" >
                                        <img class="card-img-top" src="../img/caratulas/accion/indiana.jpg"  alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Indiana</h5>
                                          <p class="card-text" >El arqueólogo, Indiana Jones, compite contra los nazis en la búsqueda de una famosa reliquia religiosa.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                </div>
                              </div>
                          </div>
                          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                              <div class="row">

                                  <div class="card-group"> 
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/terror/exorcista.jpg" height="70%"  alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">El exorcista</h5>
                                          <p class="card-text">Una actriz llama a unos sacerdotes jesuitas para que intenten terminar con la posesión demoníaca de su hija de 12 años.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/terror/rec.jpg" height="70%" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Rec</h5>
                                          <p class="card-text">Un reportero (Manuela Velasco) y su camarógrafo graban una terrible epidemia de una enfermedad que transforma a los humanos en caníbales viciosos.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/terror/elAro.jpg" height="70%" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">El Aro</h5>
                                          <p class="card-text">Una reportera debe resolver el misterio de una cinta que trae muerte a sus espectadores, antes de que sucumba a su poder.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>


                                </div>
                              </div>

                          </div>
                          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                              <div class="row">

                                  <div class="card-group"> 
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/drama/animalesNoctu.jpg" height="70%"  alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Animales Nocturnos</h5>
                                          <p class="card-text" >Susan Morrow es una mujer que tras abandonar a su primer marido, un escritor inédito, vive ahora con un médico.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/drama/everest.jpg" height="70%" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Everest</h5>
                                          <p class="card-text" >En la mañana del 10 de mayo de 1996 se desata una de las mayores tormentas de nieve que la humanidad haya visto.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>
                                      <div class="card">
                                        <img class="card-img-top" src="../img/caratulas/drama/madre.jpg" height="70%" alt="Card image cap">
                                        <div class="card-body">
                                          <h5 class="card-title">Madre</h5>
                                          <p class="card-text">La tranquila de vida de una mujer se ve alterada cuando su marido invita a su mansión a un hombre, su esposa y los dos hijos de la pareja.</p>
                                          
                                          <a href="#" class="btn btn-primary modelo">Alquiler</a>
                                        </div>
                                      </div>


                                </div>
                              </div>

                          </div>

                          
                    </div>
                </div>

                <!-- contenido -->


            </div>

            <!-- Button trigger modal -->
           
         <!-- Button trigger modal -->

<!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <img src="" id="modal_img" height="100%" width="100%">
                        </div>

                        <div class="col-6">
                            <p class="modal_contenido"></p>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <h4>Formato</h4>
                                    <select id="selFormato">
                                       <option></option>
                                    </select>

                                    <h4>Tiempo</h4>
                                    <select id="selTiempo">
                                       <option></option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <span id="precioPeliculaModal"></span>
                                </div>


                            </div>
                            
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary alquilar" data-dismiss="modal">Alquilar</button>
                  </div>
                </div>
              </div>
            </div> 

            <!-- fin modal normal-->

            
            <!-- Modal -->
            <div class="modal fade right" id="exampleModalPreview" tabindex="-1" role="dialog" aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
              <div class="modal-dialog modal-full-height modal-right" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalPreviewLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" id="modalCar">
                    ...
                  </div>
                  <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <span>Total Alquiler</span>
                            <span id="total_alquiler" class="float-right"> </span>

                        </div>

                        <div class="col-md-12">
                             <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                             <button type="button" class="btn btn-primary" id="alquilarDb">Enviar</button>

                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

<!-- Modal -->

        </div>

       
    </body>
</html>
