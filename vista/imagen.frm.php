<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Alquiler Peliculas</title>
        <!-- Estilos que implementa el index -->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
     

        <!-- Scripts que implementa el index -->
        <script src="../js/jquery.js" type="text/javascript"></script> 
       <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
        <script src="../js/bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript" src="../js/imagen.js"></script>
              
       
    </head>

    <body>
		<div class="container">
			<form  accept-charset="utf-8" id="frmImagen">
				<div class="row">
					<div class="col-md-6 form-group">
				        <label for="texto">texto</label>
				        <input type="text" class="form-control" id="texto" name="texto" placeholder="ingrese" />
			    	</div>
		          	<div class="col-md-6 form-group">
				        <label for="file">subir imagen</label>
				        <input type="file" class="form-control" id="file" name="file" required />
			    	</div>
		         

				</div>
				<div class="row">
					<div class="col-md-6">
			            <button  type="button" class="btn btn-primary" name="" id="btnAdd"> Agregar</button>
			          </div>
				</div>
			
			</form>
		</div>
    	
    </body>


</html>    