<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <!-- Estilos que implementa el index -->
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/table.css" rel="stylesheet" type="text/css"/>
        <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <!--<link href="../css/jquery.dataTables.css" rel="stylesheet" type="text/css"/>-->

        <!-- Scripts que implementa el index -->
        <script src="../js/jquery.js" type="text/javascript"></script>
        <script src="../js/bootstrap.js" type="text/javascript"></script>
        <!--<script src="../js/jquery.dataTables.js" type="text/javascript"></script>-->

    </head>
    <body class="cuerpo">
        <h1 align="center">Crear usuario</h1>
        <form id="formularioUsuario">
            <input class="cajaTexto" type="number" name="txtIdentificacion" id="txtIdentificacion" placeholder="Identificación"><br>
            <div id="mensaje"></div>
            <input class="cajaTexto" type="text" name="txtNombre" id="txtNombre" placeholder="Nombre"><br>

            <input class="cajaTexto" type="text" name="txtApellido" id="txtApellido" placeholder="Apellido"><br>

            <input class="cajaTexto" type="date" name="txtFechaNacimiento" id="txtFechaNacimiento" placeholder="Fecha nacimiento"><br>
            <input type="button" name="btnAdd" id="btnAdd" value="Agregar">
        </form>

        <!-- Tabla -->
        <div id="tablaUsuario"></div>
        <!-- Modal para modificar usuario-->
        <div class="modal fade" id="modalModificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Editar usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="txtIdClienteModalM" id="txtIdClienteModalM">                        
                        <input class="cajaTexto" type="number" name="txtIdentificacionModal" id="txtIdentificacionModal" placeholder="Identificación">
                        <input class="cajaTexto" type="text" name="txtNombreModal" id="txtNombreModal" placeholder="Nombre">
                        <input class="cajaTexto" type="text" name="txtApellidoModal" id="txtApellidoModal" placeholder="Apellido">
                        <input class="cajaTexto" type="date" name="txtFechaNacimientoModal" id="txtFechaNacimientoModal" placeholder="Fecha nacimiento">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-primary">Modificar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal para eliminar usuario-->
        <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar usuario</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="txtIdClienteModalE" name="txtIdClienteModalE" >    
                        ¿Eliminar usuario?    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" id="btnBorrar">Eliminar</button>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="../js/usuario.js"></script>
        <a href="https://icons8.com">Icon pack by Icons8</a>
    </body>
</html>
