<?php

	require_once '../entidad/tiempoalquiler.entidad.php';
	include_once '../modelo/tiempoalquiler.modelo.php';

	$retorno = array('exito' => 1, 'mensaje' => null,'arreglo'=>null);

	try {
		
	    $tiempoE = new \entidad\TiempoAlq();
	    $tiempoM = new \modelo\TiempoAlq($tiempoE);
	    //$retorno['arreglo'] = $formatoM->leer();
	    $retorno = $tiempoM->leer();

	    unset($tiempoE);
	    unset($tiempoM);
	} catch (Exception $ex) {
	    $retorno['exito'] = 0;
	    $retorno['mensaje'] = $ex->getMessage();
	}

	echo json_encode($retorno);

?>