<?php

require_once '../entidad/alquiler.entidad.php';
include_once '../modelo/alquiler.modelo.php';

$retorno = array('exito' => 1, 'mensaje' => null,'arreglo'=>null);

$array = $_POST['datos'];

	/*
	foreach ($array as $key => $value) {
		
		print_r($value);
		foreach ($value as $key => $values) {
			print_r($values['titulo']);
		} 
	} */
	
try {
	$alquilerE = new \entidad\Alquiler();
    $alquilerE->setAlquiler($array);
    $alquilerM = new \modelo\Alquiler($alquilerE);
    
    $retorno['exito'] = $alquilerM->crear();

    unset($alquilerE);
    unset($alquilerM);
} catch (Exception $ex) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $ex->getMessage();
}

echo json_encode($retorno);


?>