<?php
	
	require_once '../entidad/productora.entidad.php';
	require_once '../modelo/productora.modelo.php';

	$dato = $_GET['term'];

	//var_dump($dato);
	//return $dato;

	$retorno = array();

	try {
		$productoraE = new \entidad\Productora();
		$productoraE->setProductoraNombre($dato);
		$productoraM = new \modelo\Productora($productoraE);
		$retorno = $productoraM->leer();

		unset($productoraE);
		unset($productoraM);
		
	} catch (Exception $e) {
		$retorno['exito'] = 0;
		$retorno['mensaje'] =  $e->getMessage();
		var_dump($retorno['mensaje']);
	}
	
	echo json_encode($retorno);
		
?>