<?php
/* PASO A PASO
 * 1. Recuperar la información
 * 2. Instanciar la entidad y enviar la información por el metodo set
 * 3. Instanciar el modelo
 * 4. Pasar el objeto al modelo
 * 5. Llamar el método agregar del modelo 
 */
include_once '../entidad/usuarioEntidad.php';
require_once '../modelo/usuarioModelo.php';

$retorno = array('exito'=>1,'mensaje'=>'ok');

try {
    /* @var $_POST type */
   $identifiicacion=$_POST['txtIdClienteModalE'];
   
    $usuarioEntidad = new usuarioEntidad\Usuario();
    $usuarioEntidad->setIdentificacion($identifiicacion);
    $usuarioModelo = new usuarioModelo\Usuario($usuarioEntidad);
    $usuarioModelo->eliminarCliente($usuarioEntidad->getIdentificacion);
    
    unset($usuarioEntidad);
    unset($usuarioModelo);
    
} catch (Exception $ex) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = "Error al eliminar: ".$ex->getMessage();
}

echo json_encode($retorno);
