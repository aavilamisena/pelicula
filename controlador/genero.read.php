<?php

include_once '../entidad/genero.entidad.php';
include_once '../modelo/genero.modelo.php';

$var = $_POST;

$retorno = array();

try {

		$generoE = new \entidad\Genero();
		
		$generoM = new \modelo\Genero($generoE);
		$retorno = $generoM->Leer();

		unset($generoE);
		unset($generoM);
	
} catch (Exception $e) {
	
}

echo json_encode($retorno);