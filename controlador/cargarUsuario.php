<?php

require_once '../entidad/usuarioEntidad.php';
include_once '../modelo/usuarioModelo.php';

$retorno = array('exito' => 1, 'mensaje' => 'Ok','arreglo'=>null);

try {
    $usuarioEntidad = new \usuarioEntidad\Usuario();
    $usuarioModelo = new \usuarioModelo\Usuario($usuarioEntidad);
    $retorno['arreglo'] = $usuarioModelo->cargar();

    unset($usuarioEntidad);
    unset($usuarioModelo);
} catch (Exception $ex) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $ex->getMessage();
}

echo json_encode($retorno);
