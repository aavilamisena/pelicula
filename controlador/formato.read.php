<?php

require_once '../entidad/formato.entidad.php';
include_once '../modelo/formato.modelo.php';

$retorno = array('exito' => 1, 'mensaje' => null,'arreglo'=>null);

try {
	
    $formatoE = new \entidad\Formato();
    $formatoM = new \modelo\Formato($formatoE);
    //$retorno['arreglo'] = $formatoM->leer();
    $retorno = $formatoM->leer();

    unset($formatoE);
    unset($formatoM);
} catch (Exception $ex) {
    $retorno['exito'] = 0;
    $retorno['mensaje'] = $ex->getMessage();
}

echo json_encode($retorno);
