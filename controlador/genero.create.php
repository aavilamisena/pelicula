<?php
	
	use Exception;

	require_once '../entidad/genero.entidad.php';
	require_once '../modelo/genero.modelo.php';

	$nombreG = $_POST['genero'];
	

	$retorno = array('exito' => 1, 'mensaje' => null,'arreglo'=>null);

	try {
		$generoE = new \entidad\Genero();
		$generoE->setNombreGenero($nombreG);
		$generoE->setEstado('A');

		$generoM = new \modelo\Genero($generoE);
		$retorno['arreglo'] = $generoM->Crear();

		unset($generoE);
		unset($generoM);

		
	} catch (Exception $e) {
		$retorno['exito'] = 0;
    	$retorno['mensaje'] = $e->getMessage();
		
	}

	echo json_encode($retorno);


?>