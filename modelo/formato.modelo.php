<?php

namespace modelo;

use Exception;
use PDO;

require_once '../entidad/formato.entidad.php';
require_once '../entorno/conexion.php';

/**
* 
*/
class Formato
{
	private $id_formato;
	private $nombre_formato;
	private $porcentaje;
	//private $conn;
	public $resultado;
	private $conexion;
	private $retorno = array('exito' => null, 'mensaje' => "ok", 'arrayFormato' => null);
	
	function __construct(\entidad\Formato $formato)
	{
		$this->id_formato = $formato->getIdFormato();
		$this->nombre_formato = $formato->getNombreFormato();
		$this->porcentaje = $formato->getPorcentaje();
		$this->conexion = new \conexion();
		
	}

	public function leer() {
		try{
			$consulta = "SELECT * FROM Formato";
			$this->resultado = $this->conexion->conn->prepare($consulta);
			$this->resultado->execute();

			$this->retorno['exito'] = 1;
			$this->retorno['mensaje'] = "formato cargado con exito";
			$this->retorno['arrayFormato'] = $this->resultado->fetchAll(PDO::FETCH_ASSOC);
			return $this->retorno;

		}
		catch(Exception $e) {

			$this->retorno->exito = 0;
			$this->retorno->mensaje = "error al registrar formato ".$e->getCode();
			return $this->retorno;

		}
	}

	public function crear() {

		try{
			$consulta = "INSERT INTO Formato VALUES(NULL, ?, ?)";
			$this->resultado = $this->conexion->conn->prepare($consulta);
			$this->resultado->bindParam(1, $this->nombre_formato);
			$this->resultado->bindParam(2, $this->porcentaje);

			$this->resultado->execute();

			$this->retorno->exito = 1;
			$this->retorno->mensaje = "formato creado con exito";
			return $this->retorno;

		}
		catch(Exception $e) {

			$this->retorno->exito = 0;
			$this->retorno->mensaje = "error al registrar formato ".$e->getCode();
			return $this->retorno;

		}
	}

	

}

?>
