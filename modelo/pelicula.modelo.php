<?php

	namespace modelo;
	use Exception;
	use PDO;

	require_once '../entidad/pelicula.entidad.php';
	require_once '../entorno/conexion.php';

	/**
	* 
	*/
	class Pelicula 
	{
		private $id_pelicula;
		private $nombre;
		private $genero;
		private $imagen;
		private $formato = array();
		private $cantidad = array();
		private $estado;
		private $idProductora;

		private $conexion;
		public $resultado;

		private $retorno = array();


		
		public function __construct(\entidad\Pelicula $peliculaE)
		{
			$this->nombre = $peliculaE->getNombre();
			$this->genero = $peliculaE->getGenero();
			$this->imagen = $peliculaE->getImagen();
			$this->formato = $peliculaE->getFormato();
			$this->cantidad = $peliculaE->getCantidad();
			$this->idProductora = $peliculaE->getIdProductora();
			$this->estado = 'A';

			$this->conexion = new \conexion();

		}

		public function crearPelicula()
		{



			try {

				foreach ($this->formato as $key => $value) {
				
					// var_dump("valor del key ".$this->cantidad[$key]);
					// return 1;

					$this->resultado = $this->conexion->conn->prepare("INSERT INTO peliculaformato VALUES(NULL, :id_formato, :id_pelicula, :cantidad, :estado)");
				
					$this->resultado->bindParam(':id_formato', $value);
					$this->resultado->bindParam(':id_pelicula', $this->id_pelicula);
					$this->resultado->bindParam(':cantidad', $this->cantidad[$key]);
					$this->resultado->bindParam(':estado', $this->estado);
					$this->resultado->execute();

				
				}


				
				$this->retorno['exito'] = 1;
				$this->retorno['mensaje'] = "pelicula creada";

				

			} catch (Exception $e) {

				$this->retorno['exito'] = 1;
				$this->retorno['mensaje'] = "pelicula creada";
				
			}
			return $this->retorno;
			
		}

		public function crear()
		{
				// print_r($this->formato);
				// 	return 1;

			$variable = (int)$this->idProductora;
			try {
				$sql = "INSERT INTO pelicula VALUES(null, :nombre, :genero, :imagen, :id_productora, :estado)";
				$this->resultado = $this->conexion->conn->prepare($sql);

				$this->resultado->bindParam(':nombre', $this->nombre);
				$this->resultado->bindParam(':genero', $this->genero);
				$this->resultado->bindParam(':imagen', $this->imagen);
				
				$this->resultado->bindParam(':id_productora', $variable);
				$this->resultado->bindParam(':estado', $this->estado);

				$this->resultado->execute();

				$this->id_pelicula = $this->conexion->UltimoId();

				var_dump($this->id_pelicula);

				$this->crearPelicula();

				// return 1;

				$this->retorno['exito'] = 1;
				$this->retorno['mensaje'] = "pelicula creada";

				
			} catch (Exception $e) {
				$this->retorno['exito'] = 0;
				$this->retorno['mensaje'] = "error: ".$e->getMessage();
			}

			return $this->retorno;
		} // fin crear

		public function leer(){

			try {

				$this->result = $this->conexion->conn->prepare("SELECT * FROM vista_pelicula_formato_genero_productoras WHERE estado ='A'");

				$this->result->execute();
				$this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);

				return $this->retorno;
				
			} catch (Exception $e){
				
				$this->retorno['exito'] = 0;
				$this->retorno['mensaje'] = "ERROR:".$e->getCode();
				return $this->retorno;
			}
		}

		public function leerById($id){

			try {

				$this->result = $this->conexion->conn->prepare("SELECT * FROM vista_pelicula_formato_genero_productoras WHERE dirPelicula = :id");

				$this->result->bindParam(':id', $id);
				$this->result->execute();


				$this->retorno = $this->result->fetchAll(PDO::FETCH_ASSOC);

				// print_r($this->retorno);
				
				// return 1;

				return $this->retorno;
				
			} catch (Exception $e){
				
				$this->retorno['exito'] = 0;
				$this->retorno['mensaje'] = "ERROR:".$e->getCode();
				return $this->retorno;
			}
		}

		


	}


?>