<?php

/* PASO A PASO
 * 1. Recuperar la informacion de la entidad por medio del constructor y los metodos GET
 */

namespace usuarioModelo;

require_once '../entidad/usuarioEntidad.php';
include_once '../entorno/conexion.php';

class Usuario {

    private $identificacion;
    private $nombre;
    private $apellido;
    private $fechaNacimiento;
    
    public $conexion;
    private $sql;
    private $resulatdo;
    
    function __construct(\usuarioEntidad\Usuario $usuarioEntidad) {
        $this->identificacion = $usuarioEntidad->getIdentificacion();
        $this->nombre = $usuarioEntidad->getNombre();
        $this->apellido = $usuarioEntidad->getApellido();
        $this->fechaNacimiento = $usuarioEntidad->getFechaNacimiento();

        $this->conexion = new \conexion();
    }

    function agregar() {
        $this->sql = "INSERT INTO cliente VALUES(null,'$this->identificacion','$this->nombre',"
                . "'$this->apellido','$this->fechaNacimiento','Activo')";
        $this->conexion->ejecutar($this->sql);
    }

    function eliminarCliente($identificacion){
        $sql = "UPDATE cliente SET cliEstado='Inactivo' WHERE cliIdentificacion='$identificacion'";
        $this->conexion->ejecutar($sql);
    }
    
    function cargar() {
        $this->sql = "SELECT * FROM cliente WHERE cliEstado='Activo' ORDER BY idCliente DESC";
        $this->conexion->ejecutar($this->sql);
        $this->resulatdo = $this->conexion->obtenerArreglo();
        return $this->resulatdo;
    }

    function obtenerCliente(){
        $this->sql = "SELECT * FROM clientes WHERE cliIdentificacion='$this->identificacion'";
        $this->conexion->ejecutar($this->sql);
        $this->resulatdo = $this->conexion->obtenerArreglo();
        return $this->resulatdo;
    }
}
