<?php
	
	namespace modelo;

	use Exception;
	use PDO;

	require_once '../entorno/conexion.php';
	require_once '../entidad/genero.entidad.php';

	/**
	* 
	*/
	class Genero
	{
		private $idGenero;
		private $nombreGenero;
		private $estado;

		private $conexion;
		public $resultado;

		public $retorno = array('exito' => null, 'mensaje' => "ok", 'arrayFormato' => null);
		
		function __construct(\entidad\Genero $generoE)
		{
			$this->nombreGenero = $generoE->getNombreGenero();
			$this->estado = $generoE->getEstado();

			$this->conexion = new \conexion();

		}

		public function Crear()
		{
			//$this->conexion->conn->beginTransaction();
			$sql = "INSERT INTO genero VALUES(NULL, ?, ?)";
			try {
				var_dump("desde",$this->estado);
				$this->resultado = $this->conexion->conn->prepare($sql);

				$this->resultado->bindParam(1, $this->nombreGenero);
				$this->resultado->bindParam(2, $this->estado);

				$this->resultado->execute();

				//$this->conexion->conn->commit();

				$this->retorno['exito'] = 1;
				$this->retorno['mensaje'] = "genero guardado con exito";

				
			} catch (Exception $e) {
				//$this->conexion->conn->rollBack();
				$this->retorno['exito'] = 0;
				
				$this->retorno['mensaje'] = "error: ".$e->getCode();

			}

			return $this->retorno;
		} // fin crear

		public function Leer()
		{
			# code...
			try {
				$this->resultado =   $this->conexion->conn->prepare("SELECT * FROM genero WHERE estado = 'A'");
				$this->resultado->execute();

				$this->retorno['arrayFormato'] = $this->resultado->fetchAll(PDO::FETCH_ASSOC);
				$this->retorno['exito'] = 1;
				$this->retorno['mensaje'] = "genero guardado con exito";


				
			} catch (Exception $e) {
				$this->retorno->exito = 0;
				$this->retorno->mensaje = "error al registrar formato ".$e->getCode();
				return $this->retorno;
				
			}

			return $this->retorno;
		}
	}
?>