<?php

namespace modelo;
use Exception;
use PDO;

require_once '../entorno/conexion.php';
require_once '../entidad/productora.entidad.php';

/**
* 
*/
class Productora
{

	private $id_productora;
	private $productora_nombre;
	private $estado;

	private $conexion;
	public $resultado;

	public $productora  = array();
	public $retorno = array();

	function __construct(\entidad\Productora $productoraE)
	{
		$this->id_productora = $productoraE->getIdProductora();
		$this->productora_nombre = $productoraE->getProductoraNombre();

		$this->conexion = new \conexion();
		
	}

	function leer() {
		$sql = "SELECT * FROM productoras WHERE productora_nombre LIKE CONCAT('%',:term,'%') AND estado = 'A' ";

		try {

			$this->resultado = $this->conexion->conn->prepare("SELECT * FROM productoras WHERE productora_nombre LIKE CONCAT('%',:term,'%') AND estado = 'A' ");
			$this->resultado->bindParam(':term', $this->productora_nombre);

			$this->resultado->execute();

			
			$this->retorno = $this->resultado->fetchAll(PDO::FETCH_ASSOC);

			//var_dump($this->retorno['arrayFormato']);

			//return 1;
			foreach ($this->retorno as $key => $value) {
				
				
				$this->productora[] =  array("value" => $value['id_productora'], "label" => $value['productora_nombre']);
			}
			//$this->conexion->close();
			return $this->productora; 
			
		} catch (Exception $e) {
			$this->retorno['exito'] = 0;
			$this->retorno['mensaje'] = "error: ".$e->getCode();
		}

		
	}
}
