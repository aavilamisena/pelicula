<?php

namespace modelo;

use Exception;
use PDO;

require_once '../entidad/alquiler.entidad.php';
require_once '../entorno/conexion.php';

/**
* 
*/
class Alquiler
{
	private $id_alquiler;
    private $alquiler; // array
     public $nombre_pelicula;
     
     public $id_formato;
     public $id_tiempo;
     public $valor;
	
	public $resultado;
	private $conexion;
	private $retorno = array('exito' => null, 'mensaje' => "ok", 'arrayFormato' => null);
	
	function __construct(\entidad\Alquiler $alquilerE)
	{
		$this->id_alquiler = $alquilerE->getAlquiler();
		$this->id_formato = $alquilerE->getIdFormato();
		$this->idTiempoAlquiler = $alquilerE->getIdTiempo();
		$this->nombre_pelicula = $alquilerE->getNombrePelicula();
		$this->valor = $alquilerE->getValor();
		$this->alquiler = $alquilerE->getAlquiler();
		$this->conexion = new \conexion();
		
	}

	

	public function crear() {

		$this->conexion->conn->beginTransaction();
		try{
			//$consulta = "INSERT INTO alquilerpeliculas VALUES(NULL, :nombrePelicula, :idTiempo, :idFormato, :precio)";

			foreach ($this->alquiler as $key => $value) {
				$this->nombre_pelicula = $value['titulo'];
				$this->id_formato = $value['formato'];
				$this->id_tiempo = $value['tiempo'];
				$this->valor = $value['precio'];



				$this->resultado = $this->conexion->conn->prepare("INSERT INTO alquilerpeliculas VALUES(NULL, :nombrePelicula, :idTiempo, :idFormato, :precio)");
				$this->resultado->bindParam(':nombrePelicula', $this->nombre_pelicula);
				$this->resultado->bindParam(':idFormato', $this->id_formato);
				$this->resultado->bindParam(':idTiempo', $this->id_tiempo);
				$this->resultado->bindParam(':precio', $this->valor);
				$this->resultado->execute();
			
			}

			$this->conexion->conn->commit();
			$this->retorno['exito'] = 1;
			$this->retorno['mensaje'] = "alquiler guardado con exito";
		}

	
		catch(Exception $e) {

			$this->conexion->conn->rollBack();
			$this->retorno['exito'] = 0;
			$this->retorno['mensaje'] = "error: ".$e->getCode();
		}

		return $this->retorno;
	}

	

}

?>
