<?php

namespace modelo;

use Exception;
use PDO;

require_once '../entorno/conexion.php';
require_once '../entidad/tiempoalquiler.entidad.php';

/**
* 
*/
class TiempoAlq
{
	private $idAlquiler;
	private $descripcion;
	private $precio;

	public $resultado;
	private $conexion;
	private $retorno = array('exito' => null, 'mensaje' => "ok", 'arrayFormato' => null);
	
	function __construct(\entidad\TiempoAlq $alquiler)
	{
		$this->idAlquiler = $alquiler->getIdAlquiler();
		$this->descripcion = $alquiler->getDescripcion();
		$this->precio = $alquiler->getPrecio();

		$this->conexion = new \conexion();

	}

	public function leer() {
		try{
			$consulta = "SELECT * FROM tiempoalquiler";
			$this->resultado = $this->conexion->conn->prepare($consulta);
			$this->resultado->execute();

			$this->retorno['exito'] = 1;
			$this->retorno['mensaje'] = "formato cargado con exito";
			$this->retorno['arrayFormato'] = $this->resultado->fetchAll(PDO::FETCH_ASSOC);
			return $this->retorno;

		}
		catch(Exception $e) {

			$this->retorno->exito = 0;
			$this->retorno->mensaje = "error al registrar formato ".$e->getCode();
			return $this->retorno;

		}
	} //fin funcion leer()

	public function crear() {

		try{
			$consulta = "INSERT INTO tiempoalquiler VALUES(NULL, ?, ?)";
			$this->resultado = $this->conexion->conn->prepare($consulta);
			$this->resultado->bindParam(1, $this->descripcion);
			$this->resultado->bindParam(2, $this->precio);

			$this->resultado->execute();

			$this->retorno->exito = 1;
			$this->retorno->mensaje = "formato creado con exito";
			return $this->retorno;

		}
		catch(Exception $e) {

			$this->retorno->exito = 0;
			$this->retorno->mensaje = "error al registrar formato ".$e->getCode();
			return $this->retorno;

		}
	}

}





?>