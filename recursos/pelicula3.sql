-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-03-2019 a las 04:05:24
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pelicula3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alquilerpeliculas`
--

CREATE TABLE `alquilerpeliculas` (
  `idAlquilerPeliculas` int(11) NOT NULL,
  `nombrePelicula` varchar(60) COLLATE utf16_spanish_ci NOT NULL,
  `idTiempo` varchar(60) COLLATE utf16_spanish_ci NOT NULL,
  `idFormato` varchar(60) COLLATE utf16_spanish_ci NOT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `alquilerpeliculas`
--

INSERT INTO `alquilerpeliculas` (`idAlquilerPeliculas`, `nombrePelicula`, `idTiempo`, `idFormato`, `precio`) VALUES
(1, 'El seÃ±or de los Anillos', 'semana - 30000 ', 'DVD ', 3000),
(2, 'Cronicas de Narnia', 'dia - 5000 ', 'BLUERAY ', 1000),
(3, 'El seÃ±or de los Anillos', 'semana - 30000 ', 'DVD ', 3000),
(4, 'Cronicas de Narnia', 'dia - 5000 ', 'BLUERAY ', 1000),
(5, 'Rec', 'semana - 30000 ', 'BLUE-RAY-3D ', 9000),
(6, 'El Aro', 'dia - 5000 ', 'BLUERAY ', 1000),
(7, 'Cronicas de Narnia', 'mes - 80000 ', 'DVD ', 8000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formato`
--

CREATE TABLE `formato` (
  `id_formato` int(11) NOT NULL,
  `nombre_formato` varchar(80) COLLATE utf16_spanish_ci NOT NULL,
  `porcentaje` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `formato`
--

INSERT INTO `formato` (`id_formato`, `nombre_formato`, `porcentaje`) VALUES
(1, 'DVD', 10),
(2, 'BLUERAY', 20),
(3, 'BLUE-RAY-3D', 30);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idGenero` int(11) NOT NULL,
  `nombreGenero` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('A','I') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idGenero`, `nombreGenero`, `estado`) VALUES
(1, 'Drama', 'A'),
(2, 'Porno', 'A'),
(3, 'Accion', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `idPelicula` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf16_spanish_ci NOT NULL,
  `genero` int(11) NOT NULL,
  `imagen` varchar(60) COLLATE utf16_spanish_ci NOT NULL,
  `id_productora` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE utf16_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`idPelicula`, `nombre`, `genero`, `imagen`, `id_productora`, `estado`) VALUES
(2, 'sfddsf', 1, '../img/nissan.png', 3, 'A'),
(3, 'dalmata', 1, '../img/images.jpg', 3, 'A'),
(4, 'dalgo s', 1, '../img/bus.jpg', 3, 'A'),
(5, 'pedro paramo', 1, '../img/pedro_paramo', 3, 'A'),
(18, 'nombre', 1, '../img/nombre.PNG', 3, 'A'),
(19, 'use', 1, '../img/use.PNG', 3, 'A'),
(21, 'dfsfs', 1, '../img/dfsfs.jpg', 3, 'A'),
(22, 'sfdsffsfds', 1, '../img/sfdsffsfds.jpg', 3, 'A'),
(23, 'sfdsffds', 1, '../img/sfdsffds.jpg', 3, 'A'),
(24, 'sfds', 1, '../img/sfds.jpg', 3, 'A'),
(25, 'dos4', 1, '../img/dos4.jpg', 3, 'A'),
(26, 'prueba1', 1, '../img/prueba1.jpg', 3, 'A'),
(27, 'fdsffdsfds', 1, '../img/fdsffdsfds.PNG', 3, 'A'),
(28, 'fdsfdsf', 2, '../img/fdsfdsf.PNG', 3, 'A'),
(29, 'rrrrr', 1, '../img/rrrrr.jpg', 3, 'A'),
(30, 'fdsf', 1, '../img/fdsf.PNG', 3, 'A'),
(31, 'ultima', 1, '../img/ultima.PNG', 3, 'A'),
(32, 'ultimo1', 1, '../img/ultimo1.PNG', 3, 'A'),
(33, 'pelicula', 1, '../img/pelicula.jpg', 3, 'A'),
(34, 'hult', 2, '../img/hult.PNG', 2, 'A'),
(35, 'halcon', 1, '../img/halcon.png', 3, 'A'),
(40, 'halcon', 1, '../img/halcon.png', 3, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `peliculaformato`
--

CREATE TABLE `peliculaformato` (
  `id_pelicula_formato` int(11) NOT NULL,
  `id_formato` int(11) NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado` enum('A','I') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `peliculaformato`
--

INSERT INTO `peliculaformato` (`id_pelicula_formato`, `id_formato`, `id_pelicula`, `cantidad`, `estado`) VALUES
(1, 3, 32, 6, 'A'),
(2, 2, 33, 77, 'A'),
(3, 1, 34, 3, 'A'),
(4, 2, 34, 5, 'A'),
(5, 1, 35, 2, 'A'),
(6, 1, 36, 2, 'A'),
(7, 1, 37, 2, 'A'),
(8, 1, 38, 2, 'A'),
(9, 1, 39, 2, 'A'),
(10, 1, 40, 2, 'A'),
(11, 1, 41, 2, 'A'),
(12, 1, 42, 2, 'A'),
(13, 1, 43, 2, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoras`
--

CREATE TABLE `productoras` (
  `id_productora` int(11) NOT NULL,
  `productora_nombre` varchar(60) COLLATE latin1_spanish_ci NOT NULL,
  `estado` enum('A','I') COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productoras`
--

INSERT INTO `productoras` (`id_productora`, `productora_nombre`, `estado`) VALUES
(1, 'Sony', 'A'),
(2, 'Universal', 'A'),
(3, 'Warner', 'A'),
(4, 'Aim', 'A'),
(5, 'Equilibrium', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tiempoalquiler`
--

CREATE TABLE `tiempoalquiler` (
  `idAlquiler` int(11) NOT NULL,
  `descripcion` varchar(30) COLLATE utf16_spanish_ci NOT NULL,
  `precio` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

--
-- Volcado de datos para la tabla `tiempoalquiler`
--

INSERT INTO `tiempoalquiler` (`idAlquiler`, `descripcion`, `precio`) VALUES
(1, 'dia', 5000),
(2, 'semana', 30000),
(5, '15 dias', 50000),
(6, 'mes', 80000);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vista_pelicula_formato_genero_productoras`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vista_pelicula_formato_genero_productoras` (
`dirPelicula` int(11)
,`nombreGenero` varchar(60)
,`nombre` varchar(30)
,`imagen` varchar(60)
,`cantidad` int(11)
,`nombre_formato` varchar(80)
,`productora_nombre` varchar(60)
,`estado` enum('A','I')
,`dirGenero` int(11)
,`dirFormato` int(11)
,`dirProductora` int(11)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vista_pelicula_formato_genero_productoras`
--
DROP TABLE IF EXISTS `vista_pelicula_formato_genero_productoras`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vista_pelicula_formato_genero_productoras`  AS  select `p`.`idPelicula` AS `dirPelicula`,`g`.`nombreGenero` AS `nombreGenero`,`p`.`nombre` AS `nombre`,`p`.`imagen` AS `imagen`,`pf`.`cantidad` AS `cantidad`,`f`.`nombre_formato` AS `nombre_formato`,`pro`.`productora_nombre` AS `productora_nombre`,`p`.`estado` AS `estado`,`g`.`idGenero` AS `dirGenero`,`f`.`id_formato` AS `dirFormato`,`pro`.`id_productora` AS `dirProductora` from ((((`pelicula` `p` join `genero` `g` on((`p`.`genero` = `g`.`idGenero`))) join `peliculaformato` `pf` on((`p`.`idPelicula` = `pf`.`id_pelicula`))) join `formato` `f` on((`pf`.`id_formato` = `f`.`id_formato`))) join `productoras` `pro` on((`p`.`id_productora` = `pro`.`id_productora`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alquilerpeliculas`
--
ALTER TABLE `alquilerpeliculas`
  ADD PRIMARY KEY (`idAlquilerPeliculas`),
  ADD KEY `idFormato` (`idFormato`);

--
-- Indices de la tabla `formato`
--
ALTER TABLE `formato`
  ADD PRIMARY KEY (`id_formato`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idGenero`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`idPelicula`),
  ADD KEY `id_productora` (`id_productora`),
  ADD KEY `genero` (`genero`);

--
-- Indices de la tabla `peliculaformato`
--
ALTER TABLE `peliculaformato`
  ADD PRIMARY KEY (`id_pelicula_formato`),
  ADD KEY `id_formato` (`id_formato`),
  ADD KEY `id_pelicula` (`id_pelicula`);

--
-- Indices de la tabla `productoras`
--
ALTER TABLE `productoras`
  ADD PRIMARY KEY (`id_productora`);

--
-- Indices de la tabla `tiempoalquiler`
--
ALTER TABLE `tiempoalquiler`
  ADD PRIMARY KEY (`idAlquiler`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alquilerpeliculas`
--
ALTER TABLE `alquilerpeliculas`
  MODIFY `idAlquilerPeliculas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `formato`
--
ALTER TABLE `formato`
  MODIFY `id_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `idGenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `idPelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `peliculaformato`
--
ALTER TABLE `peliculaformato`
  MODIFY `id_pelicula_formato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `productoras`
--
ALTER TABLE `productoras`
  MODIFY `id_productora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `tiempoalquiler`
--
ALTER TABLE `tiempoalquiler`
  MODIFY `idAlquiler` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD CONSTRAINT `pelicula_ibfk_1` FOREIGN KEY (`id_productora`) REFERENCES `productoras` (`id_productora`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pelicula_ibfk_2` FOREIGN KEY (`genero`) REFERENCES `genero` (`idGenero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `peliculaformato`
--
ALTER TABLE `peliculaformato`
  ADD CONSTRAINT `peliculaformato_ibfk_1` FOREIGN KEY (`id_pelicula`) REFERENCES `pelicula` (`idPelicula`),
  ADD CONSTRAINT `peliculaformato_ibfk_2` FOREIGN KEY (`id_formato`) REFERENCES `formato` (`id_formato`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
