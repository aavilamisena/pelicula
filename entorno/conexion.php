<?php

class conexion {

    public $conn = null;
    private $recordSet = null;
    private $ultimoId;

    /**
     * Constructor de la clase
     */
    function __construct() {
        $this->conn = new PDO('mysql:dbname=pelicula3;host=localhost', 'root', '');

        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Función para ejecutar una sentencia sql
     * @param type $sql
     * @throws Exception
     */
    // Opcionales
    public function ejecutar($sql) {
        $this->recordSet = $this->conn->query($sql);

        if ($this->recordSet == false) {
            throw new Exception("Error al ejecutar la sentencia SQL: " . $this->conn->error, E_USER_ERROR);
        }
    }
    
    /**
     * Función que obtiene el objeto de la consulta
     * @return Array asociativo del objeto
     */
    //Opcional
    public function obtenerArreglo(){
        return $this->recordSet->fetchAll(PDO::FETCH_ASSOC);
    }

    public function UltimoId()
    {
        $this->ultimoId = $this->conn->prepare("SELECT LAST_INSERT_ID() as lastId");
        $this->ultimoId->execute();

        $idUlt = $this->ultimoId->fetchAll(PDO::FETCH_ASSOC);

       
        return $idUlt[0]["lastId"];
        // return 

    }

}
