<?php

namespace entidad;

/**
* 
*/
class Alquiler
{
	
	private $id_alquiler;
    private $alquiler; // array
    private $id_formato;
    private $id_tiempo;
    private $nombre_pelicula;
    private $valor;

    


    /**
     * @return mixed
     */
    public function getIdAlquiler()
    {
        return $this->id_alquiler;
    }

    /**
     * @param mixed $id_alquiler
     *
     * @return self
     */
    public function setIdAlquiler($id_alquiler)
    {
        $this->id_alquiler = $id_alquiler;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlquiler()
    {
        return $this->alquiler;
    }

    /**
     * @param mixed $alquiler
     *
     * @return self
     */
    public function setAlquiler($alquiler)
    {
        $this->alquiler = $alquiler;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdFormato()
    {
        return $this->id_formato;
    }

    /**
     * @param mixed $id_formato
     *
     * @return self
     */
    public function setIdFormato($id_formato)
    {
        $this->id_formato = $id_formato;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdTiempo()
    {
        return $this->id_tiempo;
    }

    /**
     * @param mixed $id_tiempo
     *
     * @return self
     */
    public function setIdTiempo($id_tiempo)
    {
        $this->id_tiempo = $id_tiempo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombrePelicula()
    {
        return $this->nombre_pelicula;
    }

    /**
     * @param mixed $nombre_pelicula
     *
     * @return self
     */
    public function setNombrePelicula($nombre_pelicula)
    {
        $this->nombre_pelicula = $nombre_pelicula;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     *
     * @return self
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }
}
   ?>