<?php

namespace usuarioEntidad;

class Usuario{
    private $identificacion;
    private $nombre;
    private $apellido;
    private $fechaNacimiento;
    private $estado;

    function getIdentificacion() {
        return $this->identificacion;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellido() {
        return $this->apellido;
    }

    function getFechaNacimiento() {
        return $this->fechaNacimiento;
    }

    function getEstado() {
        return $this->estado;
    }

    function setIdentificacion($identificacion) {
        $this->identificacion = $identificacion;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setApellido($apellido) {
        $this->apellido = $apellido;
    }

    function setFechaNacimiento($fechaNacimiento) {
        $this->fechaNacimiento = $fechaNacimiento;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

}