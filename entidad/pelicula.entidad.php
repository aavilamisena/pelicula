<?php

namespace entidad;

/**
* 
*/
class Pelicula
{
	private $id_pelicula;
	private $nombre;
	private $genero;
	private $imagen;
	
    private $id_productora;

    private $cantidad = array();
    private $formato = array();

    

    

    /**
     * @return mixed
     */
    public function getIdPelicula()
    {
        return $this->id_pelicula;
    }

    /**
     * @param mixed $id_pelicula
     *
     * @return self
     */
    public function setIdPelicula($id_pelicula)
    {
        $this->id_pelicula = $id_pelicula;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * @param mixed $genero
     *
     * @return self
     */
    public function setGenero($genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param mixed $imagen
     *
     * @return self
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdProductora()
    {
        return $this->id_productora;
    }

    /**
     * @param mixed $id_productora
     *
     * @return self
     */
    public function setIdProductora($id_productora)
    {
        $this->id_productora = $id_productora;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * @param mixed $cantidad
     *
     * @return self
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormato()
    {
        return $this->formato;
    }

    /**
     * @param mixed $formato
     *
     * @return self
     */
    public function setFormato($formato = array())
    {
        $this->formato = $formato;

        return $this;
    }
}

?>