<?php
	
	namespace entidad;

	/**
	* 
	*/
	class Genero
	{
		
		private $idGenero;
		private $nombreGenero;
		private $estado;

		
	
    /**
     * @return mixed
     */
    public function getIdGenero()
    {
        return $this->idGenero;
    }

    /**
     * @param mixed $idGenero
     *
     * @return self
     */
    public function setIdGenero($idGenero)
    {
        $this->idGenero = $idGenero;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreGenero()
    {
        return $this->nombreGenero;
    }

    /**
     * @param mixed $nombreGenero
     *
     * @return self
     */
    public function setNombreGenero($nombreGenero)
    {
        $this->nombreGenero = $nombreGenero;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}
?>