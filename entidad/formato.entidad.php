<?php

namespace entidad;

/**
* 
*/
class Formato
{
	
	private $id_formato;
	private $nombre_formato;
	private $porcentaje;


    /**
     * @return mixed
     */
    public function getIdFormato()
    {
        return $this->id_formato;
    }

    /**
     * @param mixed $id_formato
     *
     * @return self
     */
    public function setIdFormato($id_formato)
    {
        $this->id_formato = $id_formato;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreFormato()
    {
        return $this->nombre_formato;
    }

    /**
     * @param mixed $nombre_formato
     *
     * @return self
     */
    public function setNombreFormato($nombre_formato)
    {
        $this->nombre_formato = $nombre_formato;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * @param mixed $porcentaje
     *
     * @return self
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }
}


?>
