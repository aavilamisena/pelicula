<?php

namespace entidad;

/**
* 
*/
class TiempoAlq
{
	
	private $idAlquiler;
	private $descripcion;
	private $precio;


    /**
     * @return mixed
     */
    public function getIdAlquiler()
    {
        return $this->idAlquiler;
    }

    /**
     * @param mixed $idAlquiler
     *
     * @return self
     */
    public function setIdAlquiler($idAlquiler)
    {
        $this->idAlquiler = $idAlquiler;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param mixed $descripcion
     *
     * @return self
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * @param mixed $precio
     *
     * @return self
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }
}

?>