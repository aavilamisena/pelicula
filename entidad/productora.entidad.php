<?php

namespace entidad;

/**
* 
*/
class Productora
{
	private $id_productora;
	private $productora_nombre;
	private $estado;

	

    /**
     * @return mixed
     */
    public function getIdProductora()
    {
        return $this->id_productora;
    }

    /**
     * @param mixed $id_productora
     *
     * @return self
     */
    public function setIdProductora($id_productora)
    {
        $this->id_productora = $id_productora;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductoraNombre()
    {
        return $this->productora_nombre;
    }

    /**
     * @param mixed $productora_nombre
     *
     * @return self
     */
    public function setProductoraNombre($productora_nombre)
    {
        $this->productora_nombre = $productora_nombre;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}
